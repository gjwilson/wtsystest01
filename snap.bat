echo OFF
rem purpose here is to be able to snapshot copy all files in the tree 
rem 
rem
set CdBackup=C:\Users\nhancock\Documents\backup
set prename=mfg_wtTestSystem_
set CdName=%prename%%1.zip
set CdNas=u:\snaps
dir %CdBackup%\%prename%*
pause
7z a -xr!dist -xr!build -r %CdBackup%\%CdName% *.*

rem move  %CdBackup%\%CdName%  %CdNas%\%CdName%
move  %CdBackup%\%CdName%  %CdNas%\
echo  ** stored in  %CdBackup%\%CdName% and attempted moved to  %CdNas%