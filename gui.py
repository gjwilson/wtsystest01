# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MyFrameSysTest
###########################################################################

class MyFrameSysTest ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Hydropoint Manufacturing Test", pos = wx.DefaultPosition, size = wx.Size( 1000,600 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer_FrameSysTest = wx.BoxSizer( wx.VERTICAL )

		self.m_panelSysTest = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizerPanelSysTest = wx.BoxSizer( wx.VERTICAL )

		self.m_notebook1 = wx.Notebook( self.m_panelSysTest, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_notebook1.SetForegroundColour( wx.Colour( 255, 0, 0 ) )

		self.m_tabSysTests = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_tabSysTests.SetBackgroundColour( wx.Colour( 128, 255, 255 ) )
		self.m_tabSysTests.SetToolTip( u"Testing Specific Sysem" )

		bSizer211 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_panel41 = wx.Panel( self.m_tabSysTests, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_panel41.SetFont( wx.Font( 14, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer41 = wx.BoxSizer( wx.VERTICAL )

		self.m_DoLedsTestBtn20 = wx.Button( self.m_panel41, wx.ID_ANY, u"Leds Test", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_DoLedsTestBtn20.SetFont( wx.Font( 16, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer41.Add( self.m_DoLedsTestBtn20, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_DoBeepTestBtn21 = wx.Button( self.m_panel41, wx.ID_ANY, u"Beep Test", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_DoBeepTestBtn21.SetFont( wx.Font( 16, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer41.Add( self.m_DoBeepTestBtn21, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_DoKeysBtn22 = wx.Button( self.m_panel41, wx.ID_ANY, u"Keyboard Test", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_DoKeysBtn22.SetFont( wx.Font( 16, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer41.Add( self.m_DoKeysBtn22, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_DoRainSwitchTestBtn19 = wx.Button( self.m_panel41, wx.ID_ANY, u"Rain Switch Test", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_DoRainSwitchTestBtn19.SetFont( wx.Font( 16, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer41.Add( self.m_DoRainSwitchTestBtn19, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_DoStationTestBtn14 = wx.Button( self.m_panel41, wx.ID_ANY, u"Station Test", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_DoStationTestBtn14.SetFont( wx.Font( 16, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer41.Add( self.m_DoStationTestBtn14, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_GetFlowBtn17 = wx.Button( self.m_panel41, wx.ID_ANY, u"Flow Input Test", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_GetFlowBtn17.SetFont( wx.Font( 16, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer41.Add( self.m_GetFlowBtn17, 1, wx.ALL|wx.EXPAND, 5 )


		self.m_panel41.SetSizer( bSizer41 )
		self.m_panel41.Layout()
		bSizer41.Fit( self.m_panel41 )
		bSizer211.Add( self.m_panel41, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel42 = wx.Panel( self.m_tabSysTests, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer40 = wx.BoxSizer( wx.VERTICAL )

		self.m_SysTestConfigureSystemBtn19 = wx.Button( self.m_panel42, wx.ID_ANY, u"Configure System", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_SysTestConfigureSystemBtn19.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer40.Add( self.m_SysTestConfigureSystemBtn19, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_TestAllBtn18 = wx.Button( self.m_panel42, wx.ID_ANY, u"Do All Tests", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_TestAllBtn18.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer40.Add( self.m_TestAllBtn18, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_ReadBoardSerialNumbers19 = wx.Button( self.m_panel42, wx.ID_ANY, u"Read Board Serial Numbers", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_ReadBoardSerialNumbers19.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		bSizer40.Add( self.m_ReadBoardSerialNumbers19, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText17 = wx.StaticText( self.m_panel42, wx.ID_ANY, u"For System Test\n  Add  station & Flow Key(s)\n  Verfiy Rain switch jumper inserted\n  Add Station loading", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText17.Wrap( -1 )

		self.m_staticText17.SetFont( wx.Font( 30, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		bSizer40.Add( self.m_staticText17, 0, wx.ALL, 5 )


		self.m_panel42.SetSizer( bSizer40 )
		self.m_panel42.Layout()
		bSizer40.Fit( self.m_panel42 )
		bSizer211.Add( self.m_panel42, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_tabSysTests.SetSizer( bSizer211 )
		self.m_tabSysTests.Layout()
		bSizer211.Fit( self.m_tabSysTests )
		self.m_notebook1.AddPage( self.m_tabSysTests, u"System Tests", True )
		self.m_tabConfigure = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer17 = wx.BoxSizer( wx.VERTICAL )

		self.m_25productCode = wx.Panel( self.m_tabConfigure, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_25productCode.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_INACTIVECAPTION ) )

		sb13_productCode = wx.StaticBoxSizer( wx.StaticBox( self.m_25productCode, wx.ID_ANY, u"Product Code" ), wx.VERTICAL )

		bSizer18 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText6 = wx.StaticText( sb13_productCode.GetStaticBox(), wx.ID_ANY, u"Code1", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )

		bSizer18.Add( self.m_staticText6, 0, wx.ALL, 5 )

		self.m_confPcCode1 = wx.TextCtrl( sb13_productCode.GetStaticBox(), wx.ID_ANY, u"2", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer18.Add( self.m_confPcCode1, 0, wx.ALL, 5 )

		self.m_staticText7 = wx.StaticText( sb13_productCode.GetStaticBox(), wx.ID_ANY, u"Code2", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )

		bSizer18.Add( self.m_staticText7, 0, wx.ALL, 5 )

		self.m_confPcCode2 = wx.TextCtrl( sb13_productCode.GetStaticBox(), wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer18.Add( self.m_confPcCode2, 0, wx.ALL, 5 )


		sb13_productCode.Add( bSizer18, 1, wx.EXPAND, 5 )


		self.m_25productCode.SetSizer( sb13_productCode )
		self.m_25productCode.Layout()
		sb13_productCode.Fit( self.m_25productCode )
		bSizer17.Add( self.m_25productCode, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_26_800Num = wx.Panel( self.m_tabConfigure, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_26_800Num.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_INACTIVECAPTION ) )

		bSizer19 = wx.BoxSizer( wx.HORIZONTAL )

		sbSizer16 = wx.StaticBoxSizer( wx.StaticBox( self.m_26_800Num, wx.ID_ANY, u"1-800 Number" ), wx.VERTICAL )

		self.m_conf800Num = wx.TextCtrl( sbSizer16.GetStaticBox(), wx.ID_ANY, u"1-800-362-8774", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.m_conf800Num.SetMaxLength( 12 )
		sbSizer16.Add( self.m_conf800Num, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer19.Add( sbSizer16, 1, wx.EXPAND, 5 )


		self.m_26_800Num.SetSizer( bSizer19 )
		self.m_26_800Num.Layout()
		bSizer19.Fit( self.m_26_800Num )
		bSizer17.Add( self.m_26_800Num, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel27 = wx.Panel( self.m_tabConfigure, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_panel27.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_INACTIVECAPTION ) )

		bSizer20 = wx.BoxSizer( wx.VERTICAL )

		sbSizer17 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel27, wx.ID_ANY, u"Company Name" ), wx.VERTICAL )

		self.m_confCompanyName = wx.TextCtrl( sbSizer17.GetStaticBox(), wx.ID_ANY, u"Hydropoint Data Systems ", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		sbSizer17.Add( self.m_confCompanyName, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer20.Add( sbSizer17, 1, wx.EXPAND, 5 )


		self.m_panel27.SetSizer( bSizer20 )
		self.m_panel27.Layout()
		bSizer20.Fit( self.m_panel27 )
		bSizer17.Add( self.m_panel27, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel28 = wx.Panel( self.m_tabConfigure, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_panel28.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		self.m_panel28.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_INACTIVECAPTION ) )

		bSizer21 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_configReadBtn2 = wx.Button( self.m_panel28, wx.ID_ANY, u"Write", wx.DefaultPosition, wx.DefaultSize, 0|wx.BORDER_RAISED )
		bSizer21.Add( self.m_configReadBtn2, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_configWriteBtn3 = wx.Button( self.m_panel28, wx.ID_ANY, u"Read", wx.DefaultPosition, wx.DefaultSize, 0|wx.BORDER_RAISED )
		bSizer21.Add( self.m_configWriteBtn3, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_configSystemDetailsBtn = wx.Button( self.m_panel28, wx.ID_ANY, u"SystemDetails", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer21.Add( self.m_configSystemDetailsBtn, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_defaultEepromBtn5 = wx.Button( self.m_panel28, wx.ID_ANY, u"Default Eeprom", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer21.Add( self.m_defaultEepromBtn5, 1, wx.ALL|wx.EXPAND, 5 )


		self.m_panel28.SetSizer( bSizer21 )
		self.m_panel28.Layout()
		bSizer21.Fit( self.m_panel28 )
		bSizer17.Add( self.m_panel28, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_tabConfigure.SetSizer( bSizer17 )
		self.m_tabConfigure.Layout()
		bSizer17.Fit( self.m_tabConfigure )
		self.m_notebook1.AddPage( self.m_tabConfigure, u"Configure", False )
		self.m_tabKeyReport = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer_tabKeyReport = wx.BoxSizer( wx.HORIZONTAL )

		self.m_panelKeyReport = wx.Panel( self.m_tabKeyReport, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		gSizer_panelKeyReport = wx.GridSizer( 4, 7, 0, 0 )

		self.m_checkBoxKr04 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Run", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr04, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr03 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Rain", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr03, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr02 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Manua", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr02, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr01 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Off", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr01, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr26 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"<--", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr26, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr25 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"[+]", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr25, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox112 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBox112.Enable( False )
		self.m_checkBox112.Hide()

		gSizer_panelKeyReport.Add( self.m_checkBox112, 0, wx.ALL, 5 )

		self.m_checkBoxKr05 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Adjust", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr05, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr06 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Alerts", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr06, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr07 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Preview", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr07, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr08 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"ET", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr08, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr23 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"[-]", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr23, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr24 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"-->", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr24, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox119 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBox119.Enable( False )
		self.m_checkBox119.Hide()

		gSizer_panelKeyReport.Add( self.m_checkBox119, 0, wx.ALL, 5 )

		self.m_checkBoxKr09 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Setup", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr09, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr10 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Days", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr10, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr11 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Stations", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr11, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr12 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Copy", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr12, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr18 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"[*]", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr18, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr20 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Select", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr20, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr22 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Display", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr22, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr13 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Flow", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr13, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr14 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Comm", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr14, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr16 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Reports", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr16, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBoxKr15 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Help", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr15, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox131 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBox131.Enable( False )
		self.m_checkBox131.Hide()

		gSizer_panelKeyReport.Add( self.m_checkBox131, 0, wx.ALL, 5 )

		self.m_checkBox132 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_checkBox132.Enable( False )
		self.m_checkBox132.Hide()

		gSizer_panelKeyReport.Add( self.m_checkBox132, 0, wx.ALL, 5 )

		self.m_checkBoxKr21 = wx.CheckBox( self.m_panelKeyReport, wx.ID_ANY, u"Sound", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer_panelKeyReport.Add( self.m_checkBoxKr21, 0, wx.ALL|wx.EXPAND, 5 )


		self.m_panelKeyReport.SetSizer( gSizer_panelKeyReport )
		self.m_panelKeyReport.Layout()
		gSizer_panelKeyReport.Fit( self.m_panelKeyReport )
		bSizer_tabKeyReport.Add( self.m_panelKeyReport, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel33 = wx.Panel( self.m_tabKeyReport, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer33 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel34 = wx.Panel( self.m_panel33, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer34 = wx.BoxSizer( wx.VERTICAL )

		self.m_gaugeKeyReport = wx.Gauge( self.m_panel34, wx.ID_ANY, 24, wx.DefaultPosition, wx.DefaultSize, wx.GA_HORIZONTAL|wx.BORDER_RAISED )
		self.m_gaugeKeyReport.SetValue( 0 )
		self.m_gaugeKeyReport.SetFont( wx.Font( 20, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_gaugeKeyReport.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		self.m_gaugeKeyReport.SetToolTip( u"Progress of 24 keys to press" )
		self.m_gaugeKeyReport.SetHelpText( u"Shows number of keys test completed" )

		bSizer34.Add( self.m_gaugeKeyReport, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_staticText8 = wx.StaticText( self.m_panel34, wx.ID_ANY, u"Progress of keys pressed (24 total)", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText8.Wrap( -1 )

		bSizer34.Add( self.m_staticText8, 0, wx.ALL, 5 )


		self.m_panel34.SetSizer( bSizer34 )
		self.m_panel34.Layout()
		bSizer34.Fit( self.m_panel34 )
		bSizer33.Add( self.m_panel34, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel35 = wx.Panel( self.m_panel33, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer35 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_textCtrlKeyReport11 = wx.TextCtrl( self.m_panel35, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_RICH|wx.TE_WORDWRAP )
		self.m_textCtrlKeyReport11.SetFont( wx.Font( 30, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )
		self.m_textCtrlKeyReport11.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_HIGHLIGHTTEXT ) )

		bSizer35.Add( self.m_textCtrlKeyReport11, 1, wx.ALL|wx.EXPAND, 5 )


		self.m_panel35.SetSizer( bSizer35 )
		self.m_panel35.Layout()
		bSizer35.Fit( self.m_panel35 )
		bSizer33.Add( self.m_panel35, 1, wx.ALL|wx.EXPAND, 5 )


		self.m_panel33.SetSizer( bSizer33 )
		self.m_panel33.Layout()
		bSizer33.Fit( self.m_panel33 )
		bSizer_tabKeyReport.Add( self.m_panel33, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_tabKeyReport.SetSizer( bSizer_tabKeyReport )
		self.m_tabKeyReport.Layout()
		bSizer_tabKeyReport.Fit( self.m_tabKeyReport )
		self.m_notebook1.AddPage( self.m_tabKeyReport, u"Key Report", False )
		self.m_tabStationReport = wx.Panel( self.m_notebook1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_tabStationReport.SetFont( wx.Font( 14, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Arial" ) )

		gSizer_tabStationReport = wx.GridSizer( 5, 9, 0, 0 )

		self.m_textSr01 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"Stn1", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr01.Wrap( -1 )

		self.m_textSr01.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr01, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_textSr02 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"02", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr02.Wrap( -1 )

		self.m_textSr02.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_ITALIC, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr02, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_textSr03 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"03", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr03.Wrap( -1 )

		self.m_textSr03.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr03, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_textSr04 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"04", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr04.Wrap( -1 )

		self.m_textSr04.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr04, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_textSr05 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"05", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr05.Wrap( -1 )

		self.m_textSr05.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr05, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_textSr06 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"06", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr06.Wrap( -1 )

		self.m_textSr06.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr06, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_textSr07 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"07", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr07.Wrap( -1 )

		self.m_textSr07.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr07, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_textSr08 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"08", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr08.Wrap( -1 )

		self.m_textSr08.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr08, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_textSr09 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"09", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr09.Wrap( -1 )

		self.m_textSr09.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr09, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_textSr10 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"10", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr10.Wrap( -1 )

		self.m_textSr10.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr10, 0, wx.ALL, 5 )

		self.m_textSr11 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr11.Wrap( -1 )

		self.m_textSr11.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr11, 0, wx.ALL, 5 )

		self.m_textSr12 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr12.Wrap( -1 )

		self.m_textSr12.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr12, 0, wx.ALL, 5 )

		self.m_textSr13 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr13.Wrap( -1 )

		self.m_textSr13.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr13, 0, wx.ALL, 5 )

		self.m_textSr14 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr14.Wrap( -1 )

		self.m_textSr14.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr14, 0, wx.ALL, 5 )

		self.m_textSr15 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr15.Wrap( -1 )

		self.m_textSr15.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr15, 0, wx.ALL, 5 )

		self.m_textSr16 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr16.Wrap( -1 )

		self.m_textSr16.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr16, 0, wx.ALL, 5 )

		self.m_textSr17 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr17.Wrap( -1 )

		self.m_textSr17.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr17, 0, wx.ALL, 5 )

		self.m_textSr18 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr18.Wrap( -1 )

		self.m_textSr18.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr18, 0, wx.ALL, 5 )

		self.m_textSr19 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"19", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr19.Wrap( -1 )

		self.m_textSr19.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr19, 0, wx.ALL, 5 )

		self.m_textSr20 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr20.Wrap( -1 )

		self.m_textSr20.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr20, 0, wx.ALL, 5 )

		self.m_textSr21 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr21.Wrap( -1 )

		self.m_textSr21.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr21, 0, wx.ALL, 5 )

		self.m_textSr22 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr22.Wrap( -1 )

		self.m_textSr22.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr22, 0, wx.ALL, 5 )

		self.m_textSr23 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr23.Wrap( -1 )

		self.m_textSr23.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr23, 0, wx.ALL, 5 )

		self.m_textSr24 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr24.Wrap( -1 )

		self.m_textSr24.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr24, 0, wx.ALL, 5 )

		self.m_textSr25 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr25.Wrap( -1 )

		self.m_textSr25.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr25, 0, wx.ALL, 5 )

		self.m_textSr26 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr26.Wrap( -1 )

		self.m_textSr26.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr26, 0, wx.ALL, 5 )

		self.m_textSr27 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr27.Wrap( -1 )

		self.m_textSr27.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr27, 0, wx.ALL, 5 )

		self.m_textSr28 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"28", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr28.Wrap( -1 )

		self.m_textSr28.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr28, 0, wx.ALL, 5 )

		self.m_textSr29 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr29.Wrap( -1 )

		self.m_textSr29.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr29, 0, wx.ALL, 5 )

		self.m_textSr30 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr30.Wrap( -1 )

		self.m_textSr30.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr30, 0, wx.ALL, 5 )

		self.m_textSr31 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr31.Wrap( -1 )

		self.m_textSr31.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr31, 0, wx.ALL, 5 )

		self.m_textSr32 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr32.Wrap( -1 )

		self.m_textSr32.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr32, 0, wx.ALL, 5 )

		self.m_textSr33 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr33.Wrap( -1 )

		self.m_textSr33.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr33, 0, wx.ALL, 5 )

		self.m_textSr34 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr34.Wrap( -1 )

		self.m_textSr34.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr34, 0, wx.ALL, 5 )

		self.m_textSr35 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr35.Wrap( -1 )

		self.m_textSr35.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr35, 0, wx.ALL, 5 )

		self.m_textSr36 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSr36.Wrap( -1 )

		self.m_textSr36.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSr36, 0, wx.ALL, 5 )

		self.m_textSrMV1 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"MV1", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSrMV1.Wrap( -1 )

		self.m_textSrMV1.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSrMV1, 0, wx.ALL, 5 )

		self.m_textSrMV2 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSrMV2.Wrap( -1 )

		self.m_textSrMV2.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSrMV2, 0, wx.ALL, 5 )

		self.m_textSrMV3 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSrMV3.Wrap( -1 )

		self.m_textSrMV3.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSrMV3, 0, wx.ALL, 5 )

		self.m_textSrMV4 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSrMV4.Wrap( -1 )

		self.m_textSrMV4.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSrMV4, 0, wx.ALL, 5 )

		self.m_textSrPS1 = wx.StaticText( self.m_tabStationReport, wx.ID_ANY, u"-", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_textSrPS1.Wrap( -1 )

		self.m_textSrPS1.SetFont( wx.Font( 18, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, wx.EmptyString ) )

		gSizer_tabStationReport.Add( self.m_textSrPS1, 0, wx.ALL, 5 )


		self.m_tabStationReport.SetSizer( gSizer_tabStationReport )
		self.m_tabStationReport.Layout()
		gSizer_tabStationReport.Fit( self.m_tabStationReport )
		self.m_notebook1.AddPage( self.m_tabStationReport, u"Station Report", False )

		bSizerPanelSysTest.Add( self.m_notebook1, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panelSysTest.SetSizer( bSizerPanelSysTest )
		self.m_panelSysTest.Layout()
		bSizerPanelSysTest.Fit( self.m_panelSysTest )
		bSizer_FrameSysTest.Add( self.m_panelSysTest, 2, wx.EXPAND |wx.ALL, 5 )

		self.m_panel29 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer25 = wx.BoxSizer( wx.VERTICAL )

		self.m_uiConsole1 = wx.TextCtrl( self.m_panel29, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_RICH|wx.TE_WORDWRAP|wx.FULL_REPAINT_ON_RESIZE|wx.VSCROLL )
		self.m_uiConsole1.SetFont( wx.Font( 20, wx.FONTFAMILY_SWISS, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Calibri" ) )
		self.m_uiConsole1.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		self.m_uiConsole1.SetMinSize( wx.Size( 1400,3000 ) )

		bSizer25.Add( self.m_uiConsole1, 0, wx.ALL, 5 )


		self.m_panel29.SetSizer( bSizer25 )
		self.m_panel29.Layout()
		bSizer25.Fit( self.m_panel29 )
		bSizer_FrameSysTest.Add( self.m_panel29, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer_FrameSysTest )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_notebook1.Bind( wx.EVT_NOTEBOOK_PAGE_CHANGED, self.OnNotebook1PageChanged )
		self.m_notebook1.Bind( wx.EVT_NOTEBOOK_PAGE_CHANGING, self.OnNotebook1PageChanging )
		self.m_DoLedsTestBtn20.Bind( wx.EVT_BUTTON, self.OnDoLedsTestBtn )
		self.m_DoBeepTestBtn21.Bind( wx.EVT_BUTTON, self.OnDoBeepTestBtn )
		self.m_DoKeysBtn22.Bind( wx.EVT_BUTTON, self.OnDoKeysTestBtn )
		self.m_DoRainSwitchTestBtn19.Bind( wx.EVT_BUTTON, self.OnRainSwitchTestBtn )
		self.m_DoStationTestBtn14.Bind( wx.EVT_BUTTON, self.OnDoStationTestBtn )
		self.m_GetFlowBtn17.Bind( wx.EVT_BUTTON, self.OnGetFlowBtn )
		self.m_SysTestConfigureSystemBtn19.Bind( wx.EVT_BUTTON, self.OnSysTestConfigureSystemBtn )
		self.m_TestAllBtn18.Bind( wx.EVT_BUTTON, self.OnTestAllBtn )
		self.m_ReadBoardSerialNumbers19.Bind( wx.EVT_BUTTON, self.OnReadBoardSerialNumbers )
		self.m_configReadBtn2.Bind( wx.EVT_BUTTON, self.OnConfigureWriteBtn )
		self.m_configWriteBtn3.Bind( wx.EVT_BUTTON, self.OnConfigureReadBtn )
		self.m_configSystemDetailsBtn.Bind( wx.EVT_BUTTON, self.OnConfigureSystemDetailsBtn )
		self.m_defaultEepromBtn5.Bind( wx.EVT_BUTTON, self.OnDefaultEepromBtn )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnNotebook1PageChanged( self, event ):
		event.Skip()

	def OnNotebook1PageChanging( self, event ):
		event.Skip()

	def OnDoLedsTestBtn( self, event ):
		event.Skip()

	def OnDoBeepTestBtn( self, event ):
		event.Skip()

	def OnDoKeysTestBtn( self, event ):
		event.Skip()

	def OnRainSwitchTestBtn( self, event ):
		event.Skip()

	def OnDoStationTestBtn( self, event ):
		event.Skip()

	def OnGetFlowBtn( self, event ):
		event.Skip()

	def OnSysTestConfigureSystemBtn( self, event ):
		event.Skip()

	def OnTestAllBtn( self, event ):
		event.Skip()

	def OnReadBoardSerialNumbers( self, event ):
		event.Skip()

	def OnConfigureWriteBtn( self, event ):
		event.Skip()

	def OnConfigureReadBtn( self, event ):
		event.Skip()

	def OnConfigureSystemDetailsBtn( self, event ):
		event.Skip()

	def OnDefaultEepromBtn( self, event ):
		event.Skip()


###########################################################################
## Class MyFrameConfigureDel
###########################################################################

class MyFrameConfigureDel ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Hydropoint Manufacturing Test", pos = wx.DefaultPosition, size = wx.Size( 1200,700 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer14 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel19 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer15 = wx.BoxSizer( wx.VERTICAL )

		self.m_notebook2 = wx.Notebook( self.m_panel19, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_23configure = wx.Panel( self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer17 = wx.BoxSizer( wx.VERTICAL )

		self.m_25productCode = wx.Panel( self.m_23configure, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_25productCode.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_INACTIVECAPTION ) )

		sb13_productCode = wx.StaticBoxSizer( wx.StaticBox( self.m_25productCode, wx.ID_ANY, u"Product Code" ), wx.VERTICAL )

		bSizer18 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText6 = wx.StaticText( sb13_productCode.GetStaticBox(), wx.ID_ANY, u"Code1", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )

		bSizer18.Add( self.m_staticText6, 0, wx.ALL, 5 )

		self.m_confPcCode1 = wx.TextCtrl( sb13_productCode.GetStaticBox(), wx.ID_ANY, u"2", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer18.Add( self.m_confPcCode1, 0, wx.ALL, 5 )

		self.m_staticText7 = wx.StaticText( sb13_productCode.GetStaticBox(), wx.ID_ANY, u"Code2", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )

		bSizer18.Add( self.m_staticText7, 0, wx.ALL, 5 )

		self.m_confPcCode2 = wx.TextCtrl( sb13_productCode.GetStaticBox(), wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		bSizer18.Add( self.m_confPcCode2, 0, wx.ALL, 5 )


		sb13_productCode.Add( bSizer18, 1, wx.EXPAND, 5 )


		self.m_25productCode.SetSizer( sb13_productCode )
		self.m_25productCode.Layout()
		sb13_productCode.Fit( self.m_25productCode )
		bSizer17.Add( self.m_25productCode, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_26_800Num = wx.Panel( self.m_23configure, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_26_800Num.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_INACTIVECAPTION ) )

		bSizer19 = wx.BoxSizer( wx.HORIZONTAL )

		sbSizer16 = wx.StaticBoxSizer( wx.StaticBox( self.m_26_800Num, wx.ID_ANY, u"1-800 Number" ), wx.VERTICAL )

		self.m_conf800Num = wx.TextCtrl( sbSizer16.GetStaticBox(), wx.ID_ANY, u"1-800-362-8774", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.m_conf800Num.SetMaxLength( 12 )
		sbSizer16.Add( self.m_conf800Num, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer19.Add( sbSizer16, 1, wx.EXPAND, 5 )


		self.m_26_800Num.SetSizer( bSizer19 )
		self.m_26_800Num.Layout()
		bSizer19.Fit( self.m_26_800Num )
		bSizer17.Add( self.m_26_800Num, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel27 = wx.Panel( self.m_23configure, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_panel27.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_INACTIVECAPTION ) )

		bSizer20 = wx.BoxSizer( wx.VERTICAL )

		sbSizer17 = wx.StaticBoxSizer( wx.StaticBox( self.m_panel27, wx.ID_ANY, u"Company Name" ), wx.VERTICAL )

		self.m_confCompanyName = wx.TextCtrl( sbSizer17.GetStaticBox(), wx.ID_ANY, u"Hydropoint Data Systems ", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		sbSizer17.Add( self.m_confCompanyName, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer20.Add( sbSizer17, 1, wx.EXPAND, 5 )


		self.m_panel27.SetSizer( bSizer20 )
		self.m_panel27.Layout()
		bSizer20.Fit( self.m_panel27 )
		bSizer17.Add( self.m_panel27, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel28 = wx.Panel( self.m_23configure, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_panel28.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		self.m_panel28.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_INACTIVECAPTION ) )

		bSizer21 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_configReadBtn2 = wx.Button( self.m_panel28, wx.ID_ANY, u"Write", wx.DefaultPosition, wx.DefaultSize, 0|wx.BORDER_RAISED )
		bSizer21.Add( self.m_configReadBtn2, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_configWriteBtn3 = wx.Button( self.m_panel28, wx.ID_ANY, u"Read", wx.DefaultPosition, wx.DefaultSize, 0|wx.BORDER_RAISED )
		bSizer21.Add( self.m_configWriteBtn3, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_configSystemBtn = wx.Button( self.m_panel28, wx.ID_ANY, u"SystemDetails", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer21.Add( self.m_configSystemBtn, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_defaultEepromBtn5 = wx.Button( self.m_panel28, wx.ID_ANY, u"Default Eeprom", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer21.Add( self.m_defaultEepromBtn5, 1, wx.ALL|wx.EXPAND, 5 )


		self.m_panel28.SetSizer( bSizer21 )
		self.m_panel28.Layout()
		bSizer21.Fit( self.m_panel28 )
		bSizer17.Add( self.m_panel28, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_23configure.SetSizer( bSizer17 )
		self.m_23configure.Layout()
		bSizer17.Fit( self.m_23configure )
		self.m_notebook2.AddPage( self.m_23configure, u"Configure", True )
		self.m_24sysTests = wx.Panel( self.m_notebook2, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.m_24sysTests.SetBackgroundColour( wx.Colour( 128, 255, 255 ) )
		self.m_24sysTests.SetToolTip( u"Testing Specific Sysem" )

		bSizer211 = wx.BoxSizer( wx.VERTICAL )

		self.m_staticText12 = wx.StaticText( self.m_24sysTests, wx.ID_ANY, u"To De Defined System Test Funcations", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText12.Wrap( -1 )

		bSizer211.Add( self.m_staticText12, 0, wx.ALL, 5 )


		self.m_24sysTests.SetSizer( bSizer211 )
		self.m_24sysTests.Layout()
		bSizer211.Fit( self.m_24sysTests )
		self.m_notebook2.AddPage( self.m_24sysTests, u"System Tests", False )

		bSizer15.Add( self.m_notebook2, 1, wx.EXPAND |wx.ALL, 5 )


		self.m_panel19.SetSizer( bSizer15 )
		self.m_panel19.Layout()
		bSizer15.Fit( self.m_panel19 )
		bSizer14.Add( self.m_panel19, 1, wx.EXPAND |wx.ALL, 5 )

		self.m_panel29 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer25 = wx.BoxSizer( wx.VERTICAL )

		self.m_uiConsole1 = wx.TextCtrl( self.m_panel29, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_WORDWRAP|wx.FULL_REPAINT_ON_RESIZE|wx.VSCROLL )
		self.m_uiConsole1.SetFont( wx.Font( 30, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL, False, "Calibri" ) )
		self.m_uiConsole1.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )

		bSizer25.Add( self.m_uiConsole1, 1, wx.ALL|wx.EXPAND, 5 )


		self.m_panel29.SetSizer( bSizer25 )
		self.m_panel29.Layout()
		bSizer25.Fit( self.m_panel29 )
		bSizer14.Add( self.m_panel29, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( bSizer14 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_configReadBtn2.Bind( wx.EVT_BUTTON, self.OnConfigureWriteBtn )
		self.m_configWriteBtn3.Bind( wx.EVT_BUTTON, self.OnConfigureReadBtn )
		self.m_configSystemBtn.Bind( wx.EVT_BUTTON, self.OnConfigureSystemBtn )
		self.m_defaultEepromBtn5.Bind( wx.EVT_BUTTON, self.OnDefaultEepromBtn )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnConfigureWriteBtn( self, event ):
		event.Skip()

	def OnConfigureReadBtn( self, event ):
		event.Skip()

	def OnConfigureSystemBtn( self, event ):
		event.Skip()

	def OnDefaultEepromBtn( self, event ):
		event.Skip()


###########################################################################
## Class MyPanelKeyboardDel
###########################################################################

class MyPanelKeyboardDel ( wx.Panel ):

	def __init__( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 428,300 ), style = wx.TAB_TRAVERSAL, name = wx.EmptyString ):
		wx.Panel.__init__ ( self, parent, id = id, pos = pos, size = size, style = style, name = name )

		gSizer2 = wx.GridSizer( 4, 7, 0, 0 )

		self.m_radioBtn11 = wx.RadioButton( self, wx.ID_ANY, u"Run", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_radioBtn11, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn8 = wx.RadioButton( self, wx.ID_ANY, u"Rain", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_radioBtn8, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn9 = wx.RadioButton( self, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_radioBtn9, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn10 = wx.RadioButton( self, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_radioBtn10, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn11 = wx.RadioButton( self, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_radioBtn11, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn12 = wx.RadioButton( self, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_radioBtn12, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn13 = wx.RadioButton( self, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_radioBtn13, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn21 = wx.RadioButton( self, wx.ID_ANY, u"Adjust", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.m_radioBtn21, 1, wx.ALL|wx.EXPAND, 5 )


		self.SetSizer( gSizer2 )
		self.Layout()

	def __del__( self ):
		pass


###########################################################################
## Class DialogKeyboard2Del
###########################################################################

class DialogKeyboard2Del ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.DefaultSize, style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer21 = wx.BoxSizer( wx.VERTICAL )

		self.m_panel19 = wx.Panel( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		gSizer4 = wx.GridSizer( 4, 7, 0, 0 )

		self.m_checkBox1 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Run", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox1, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox2 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Rain", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox2, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox3 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Manual", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox3, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox4 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox4, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn9 = wx.RadioButton( self.m_panel19, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_radioBtn9, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn10 = wx.RadioButton( self.m_panel19, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_radioBtn10, 0, wx.ALL|wx.EXPAND, 5 )

		m_radioBox9Choices = [ u"Radio Button" ]
		self.m_radioBox9 = wx.RadioBox( self.m_panel19, wx.ID_ANY, u"wxRadioBox", wx.DefaultPosition, wx.DefaultSize, m_radioBox9Choices, 1, wx.RA_SPECIFY_COLS )
		self.m_radioBox9.SetSelection( 0 )
		self.m_radioBox9.Enable( False )
		self.m_radioBox9.Hide()

		gSizer4.Add( self.m_radioBox9, 0, wx.ALL, 5 )

		self.m_checkBox = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Adjust", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox6 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox6, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox7 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox7, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox8 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox8, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn11 = wx.RadioButton( self.m_panel19, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_radioBtn11, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn12 = wx.RadioButton( self.m_panel19, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_radioBtn12, 0, wx.ALL|wx.EXPAND, 5 )

		m_radioBox10Choices = [ u"Radio Button" ]
		self.m_radioBox10 = wx.RadioBox( self.m_panel19, wx.ID_ANY, u"wxRadioBox", wx.DefaultPosition, wx.DefaultSize, m_radioBox10Choices, 1, wx.RA_SPECIFY_COLS )
		self.m_radioBox10.SetSelection( 0 )
		self.m_radioBox10.Enable( False )
		self.m_radioBox10.Hide()

		gSizer4.Add( self.m_radioBox10, 0, wx.ALL, 5 )

		self.m_checkBox9 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Setup", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox9, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox10 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox10, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox11 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox11, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox12 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox12, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn13 = wx.RadioButton( self.m_panel19, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_radioBtn13, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn14 = wx.RadioButton( self.m_panel19, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_radioBtn14, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_radioBtn15 = wx.RadioButton( self.m_panel19, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_radioBtn15, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox13 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Flow", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox13, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox14 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox14, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox15 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox15, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_checkBox16 = wx.CheckBox( self.m_panel19, wx.ID_ANY, u"Check Me!", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_checkBox16, 0, wx.ALL|wx.EXPAND, 5 )

		m_radioBox11Choices = [ u"Radio Button" ]
		self.m_radioBox11 = wx.RadioBox( self.m_panel19, wx.ID_ANY, u"wxRadioBox", wx.DefaultPosition, wx.DefaultSize, m_radioBox11Choices, 1, wx.RA_SPECIFY_COLS )
		self.m_radioBox11.SetSelection( 0 )
		self.m_radioBox11.Enable( False )
		self.m_radioBox11.Hide()

		gSizer4.Add( self.m_radioBox11, 0, wx.ALL, 5 )

		m_radioBox12Choices = [ u"Radio Button" ]
		self.m_radioBox12 = wx.RadioBox( self.m_panel19, wx.ID_ANY, u"wxRadioBox", wx.DefaultPosition, wx.DefaultSize, m_radioBox12Choices, 1, wx.RA_SPECIFY_COLS )
		self.m_radioBox12.SetSelection( 0 )
		self.m_radioBox12.Enable( False )
		self.m_radioBox12.Hide()

		gSizer4.Add( self.m_radioBox12, 0, wx.ALL, 5 )

		self.m_radioBtn16 = wx.RadioButton( self.m_panel19, wx.ID_ANY, u"RadioBtn", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer4.Add( self.m_radioBtn16, 0, wx.ALL|wx.EXPAND, 5 )


		self.m_panel19.SetSizer( gSizer4 )
		self.m_panel19.Layout()
		gSizer4.Fit( self.m_panel19 )
		bSizer21.Add( self.m_panel19, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_gaugeKeyboard2 = wx.Gauge( self, wx.ID_ANY, 24, wx.DefaultPosition, wx.DefaultSize, wx.GA_HORIZONTAL )
		self.m_gaugeKeyboard2.SetValue( 0 )
		bSizer21.Add( self.m_gaugeKeyboard2, 0, wx.ALL|wx.EXPAND, 5 )

		bSizer22 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button17 = wx.Button( self, wx.ID_ANY, u"Keys OK", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer22.Add( self.m_button17, 1, wx.ALL, 5 )

		self.m_button18 = wx.Button( self, wx.ID_ANY, u"One or more Keys FAIL", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer22.Add( self.m_button18, 1, wx.ALL, 5 )


		bSizer21.Add( bSizer22, 1, wx.EXPAND, 5 )


		self.SetSizer( bSizer21 )
		self.Layout()
		bSizer21.Fit( self )

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button17.Bind( wx.EVT_BUTTON, self.OnKeyOK )
		self.m_button18.Bind( wx.EVT_BUTTON, self.OnKeyFail )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnKeyOK( self, event ):
		event.Skip()

	def OnKeyFail( self, event ):
		event.Skip()


