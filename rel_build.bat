rem \Users\nhancock\Documents\ws\mfg_wtsystest01\wtsystest01\rel_build.bat cmd
rem converts py files to .exe and makes a build in dist
pyinstaller -y --clean  --icon=hydropointMiconA.ico wtSysTest.py
xcopy /E serial\* dist\wtSysTest\serial\*
xcopy /E windows_shortcuts\* dist\wtSysTest\windows_shortcuts\*
xcopy    README.md  dist\wtSysTest
xcopy    hydropoint-manufacturing.cfg dist\wtSysTest
